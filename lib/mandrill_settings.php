<?php 

add_action( 'admin_menu', 'neuralab_mandrill_settings_init' );
 

function neuralab_mandrill_settings_init() {
    add_options_page( __( 'Mandrill Emailer Settings', 'neuralab_mandrill' ), 
        __( 'Mandrill Emailer', 'neuralab_mandrill' ),
        'manage_options', 
        'mandrill-emailer',
        'neuralab_mandrill_settings_page'
    );
 
 
    add_settings_section(
        'neuralab_mandrill_setting_section_general',
        __( 'General Settings', 'neuralab_mandrill' ),
        'neuralab_mandrill_settings_section_callback_general',
        'mandrill-emailer'
    );
 

     
    add_settings_field(
        'neuralab_mandrill_use_mandrill',
        __( 'Use Mandrill for email', 'neuralab_mandrill' ),
        'neuralab_mandrill_setting_callback_use_mandrill',
        'mandrill-emailer',
        'neuralab_mandrill_setting_section_general'
    );  
    register_setting( 'mandrill-emailer', 'neuralab_mandrill_use_mandrill' );
 
    add_settings_field(
        'neuralab_mandrill_username',
        __( 'Mandrill user name', 'neuralab_mandrill' ),
        'neuralab_mandrill_setting_callback_username',
        'mandrill-emailer',
        'neuralab_mandrill_setting_section_general'
    ); 
    register_setting( 'mandrill-emailer', 'neuralab_mandrill_username' );
 
    add_settings_field(
        'neuralab_mandrill_api_key',
        __( 'Mandrill API key', 'neuralab_mandrill' ),
        'neuralab_mandrill_setting_callback_api_key',
        'mandrill-emailer',
        'neuralab_mandrill_setting_section_general'
    );  
    register_setting( 'mandrill-emailer', 'neuralab_mandrill_api_key' );
 
    add_settings_field(
        'neuralab_mandrill_from_name',
        __( 'Mandrill "From" name', 'neuralab_mandrill' ),
        'neuralab_mandrill_setting_callback_from_name',
        'mandrill-emailer',
        'neuralab_mandrill_setting_section_general'
    ); 
    register_setting( 'mandrill-emailer', 'neuralab_mandrill_from_name' );
 
    add_settings_field(
        'neuralab_mandrill_from_email',
        __( 'Mandrill "From" email', 'neuralab_mandrill' ),
        'neuralab_mandrill_setting_callback_from_email',
        'mandrill-emailer',
        'neuralab_mandrill_setting_section_general'
    );
    register_setting( 'mandrill-emailer', 'neuralab_mandrill_from_email' );
   
    add_settings_field(
        'neuralab_mandrill_host',
        __( 'Mandrill Host', 'neuralab_mandrill' ),
        'neuralab_mandrill_setting_host',
        'mandrill-emailer',
        'neuralab_mandrill_setting_section_general'
    );
    register_setting( 'mandrill-emailer', 'neuralab_mandrill_host' );
  
  add_settings_field(
        'neuralab_mandrill_port',
        __( 'Mandrill Port', 'neuralab_mandrill' ),
        'neuralab_mandrill_setting_port',
        'mandrill-emailer',
        'neuralab_mandrill_setting_section_general'
    );
    register_setting( 'mandrill-emailer', 'neuralab_mandrill_port' );
}

function neuralab_mandrill_settings_page() {
     if(isset($_POST['neuralab_recepeint_email']) && isset($_POST['neuralab_subject_of_email'])){
    send_test_mail($_POST['neuralab_recepeint_email'],$_POST['neuralab_subject_of_email'],$_POST['neuralab_content_of_email']);
  }
?>
    <div class="wrap">
        <div id="icon-themes" class="icon32"></div>
        <h2>Mandrill Emailer Settings</h2>
 
        <?php settings_errors(); ?>
  
        <form method="post" action="options.php">
            <?php settings_fields( 'mandrill-emailer' ); ?>
            <?php do_settings_sections( 'mandrill-emailer' ); ?>
            <?php submit_button(); ?>
        </form>
        <form  method="post">
        <table class="form-table">
          <tr><th><h3>Send test mail:</h3></th></tr>
           <tr><th>Recepeint email:</th><td><input type="text" name="neuralab_recepeint_email" class="regular-text" placeholder="insert recepeint of test email"></td></tr>
          <tr><th>Subject:</th><td><input type="text" name="neuralab_subject_of_email" class="regular-text" placeholder="insert subject of email here"></td></tr>
          <tr><th>Content:</th><td><textarea rows="5" cols="60" name="neuralab_content_of_email" placeholder="insert content of test email here" ></textarea></td></tr>
          <tr><th><input class="button button-primary" type="submit" value="Send email"></th></tr>
      </table>
          </form>
    </div>
<?php
}
function send_test_mail($to,$subject,$content){
  if($to!=="" && $subject!=="" && $content!==""){
   
  wp_mail($to,$subject,$content);
    echo 'poslali ste mail';
 }
  else{
    echo 'niste unijeli sve podatke, pokusajte ponovo!';
  }
 
}
function neuralab_mandrill_settings_section_callback_general() {
    echo '<p>Mandrill API configuration.</p>';
}

function neuralab_mandrill_setting_callback_use_mandrill() {
  echo '<input name="neuralab_mandrill_use_mandrill" id="neuralab_mandrill_use_mandrill" '
    . 'type="checkbox" value="1" class="code" '
    . checked(1, get_option('neuralab_mandrill_use_mandrill' ), false) . ' />';
}

function neuralab_mandrill_setting_callback_username() {
  echo '<input name="neuralab_mandrill_username" id="neuralab_mandrill_username" '
    . 'class="regular-text" type="text" value="' . get_option( 'neuralab_mandrill_username' ) . '" />';
}
function neuralab_mandrill_setting_callback_api_key() {
    echo '<input name="neuralab_mandrill_api_key" id="neuralab_mandrill_api_key" '
       . 'class="regular-text" type="text" value="' . get_option( 'neuralab_mandrill_api_key' ) . '" />';
}

function neuralab_mandrill_setting_callback_from_name() {
    echo '<input name="neuralab_mandrill_from_name" id="neuralab_mandrill_from_name" '
      . 'class="regular-text" type="text" value="' . get_option( 'neuralab_mandrill_from_name' ) . '"/>';
}
function neuralab_mandrill_setting_callback_from_email() {
  echo '<input name="neuralab_mandrill_from_email" id="neuralab_mandrill_from_email" '
    . 'class="regular-text" type="text" value="' . get_option( 'neuralab_mandrill_from_email' ) . '"/>';
}
function neuralab_mandrill_setting_host() {
  echo '<input name="neuralab_mandrill_host" id="neuralab_mandrill_host" '
    . 'class="regular-text" type="text" value="' . get_option( 'neuralab_mandrill_host', "smtp.mandrillapp.com" ) . '"/>';
}
function neuralab_mandrill_setting_port() {
  echo '<input name="neuralab_mandrill_port" id="neuralab_mandrill_port" '
    . 'class="regular-text" type="text" value="' . get_option( 'neuralab_mandrill_port', 587 ) . '"/>';
}
?>