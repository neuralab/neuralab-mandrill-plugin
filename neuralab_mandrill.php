<?php
/**
 * Plugin Name: Neuralab mandrill
 * Plugin URI: https://neuralab.net
 * Description: Neuralab mandrill SMTP connector
 * Version: 1.0.0
 * Author: Alen Meštrov
 * Author URI: https://neuralab.net
 * License: GPL2
 */


if(!defined('ABSPATH')) exit; // Exit if accessed directly

require_once 'lib/Mandrill.php';
require_once 'lib/mandrill_settings.php';

if(get_option('neuralab_mandrill_use_mandrill')) {
        add_action( 'phpmailer_init', 'neuralab_mandrill_phpmailer_init');
}
function neuralab_mandrill_phpmailer_init($mailer) {
 
$mailer->isSMTP();
    $mailer->SMTPAuth = true;
    $mailer->SMTPSecure ='tls';
     
    $mailer->Host = get_option('neuralab_mandrill_host');
    $mailer->Port = get_option('neuralab_mandrill_port');
  
   
    $mailer->Username = get_option('neuralab_mandrill_username');
    $mailer->Password = get_option('neuralab_mandrill_api_key');
  
   
    $from_name = get_option('neuralab_mandrill_from_name');
    if(!isset( $from_name)) {
        $from_name = 'WordPress';
    }
 
    $from_email = get_option('neuralab_mandrill_from_email');        
    if(!isset($from_email)) {
        
        $sitename = strtolower( $_SERVER['SERVER_NAME'] );
        if('www.' == substr($sitename, 0, 4)) {
            $sitename = substr($sitename, 4);
        }
         
        $from_email = 'wordpress@' . $sitename;
    }
     
    $mailer->From = $from_email;
    $mailer->FromName = $from_name;
   }



?>